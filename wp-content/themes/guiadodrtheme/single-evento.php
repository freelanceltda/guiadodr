<?php include('header.php'); ?>			    
	<div class="row col-lg-12 col-xs-12 single-eventos">
		<?php
			 the_post();
			 $postID = $post->ID; 
	         $conteudo_event = get_field('conteudo_evento', $postID);
			 $imagem_event = get_field('imagem_evento', $postID);
			 $imagem_event_bkg = get_field('imagem_background_evento', $postID);

	    ?>
		<div class="row container center">
			<div class="single-eventos--container">
				<div class="single-title work-sans-regular">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>			
		</div>
	    <div class="container center">
			<div class="row single-bkg">
				<img src="<?php echo $imagem_event_bkg; ?>">
			</div>
			<div class="row single-content  work-sans-light">
				<p><?php echo $conteudo_event; ?></p>
			</div>
		    <div class="row carousel-eventos">
		    	<div class="container">
			    	 <div id="carousel-eventos">
			    	 	<?php
						 	// loop through the rows of data
						    while ( have_rows('carousel_eventos') ) : the_row();
						    	$postID_Carousel = $post->ID; 
						    	$imagem_carousel = get_sub_field('imagem_carousel_eventos');

						 ?>
						 	<div class="carousel-img container"  data-image="<?php echo $imagem_carousel; ?>">
						 		<img src="<?php echo $imagem_carousel; ?>" id="image-<?php echo $postID_Carousel; ?>">
						 	</div>		
						 <?php endwhile; ?>
			    	 </div>
		    	</div>
		    </div>
	    </div>
	</div>



<?php include('seja.php'); ?>
<?php include('footer.php'); ?>
<?php include('modal.php'); ?>