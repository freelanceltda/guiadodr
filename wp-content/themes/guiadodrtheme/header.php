
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta content="True" name="HandheldFriendly">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="viewport" content="width=device-width">
    <!--[if lt IE 9]>      
        <script type="text/javascript" src="js/html5shiv.min.js"></script>  
    <![endif]-->
    <script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory')?>/css/nora1.0.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory')?>/css/style.css">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
</head>
<body>
    <div class="row col-lg-12 col-xs-12 col-md-12 header">
        <div class="mobile-hide tablet-hide">   
            <div class="header-info">
                <div class="container center">
                    <?php 
                        $post = get_post('155');
                        $postId = $post->ID;
                        $icon_end = get_field('icone_header_end', $postId);
                        $end = get_field('endereco_header', $postId);
                        $icon_phone = get_field('icone_header_phone', $postId);
                        $phone = get_field('telefone_header', $postId);
                        $icon_email = get_field('icone_header_email', $postId);
                        $email = get_field('email_header', $postId);
                        $icon_social = get_field('icone_social', $postId);
                        $link_social = get_field('link_rede_social', $postId);
                    ?>

                        <div class="left col-lg-12 col-md-12">
                            <div class="row col-lg-7 col-md-11 left aling  work-sans-regular">
                                <div class="header-info--end col-lg-4 col-md-3 left">
                                    <i class="<?php echo $icon_end; ?>"></i><p><?php echo $end; ?></p>
                                </div>
                                <div class="header-info--phone col-lg-3 left"> 
                                    <i class="<?php echo $icon_phone; ?>"></i><p><?php echo $phone; ?></p>
                                </div>
                                <div class="header-info--email col-lg-4 left">
                                     <i class="<?php echo $icon_email; ?>"></i><p><?php echo $email; ?></p>
                                </div>
                            </div>
                            <div class="header-info--social col-lg-1 col-md-1 right text-right container">
                                <a href="<?php echo $link_social; ?>"><i class="<?php echo $icon_social; ?>"></i></a>
                            </div>
                        </div>
                </div>   
            </div>
            <div class="header-list">
                <div class="container center">
                    <div class="left col-lg-1">
                        <div class="header-list--logo">
                            <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory')?>/image/logo.png"></a>
                        </div>
                    </div>
                    <div class="right col-lg-11">
                        <div class="header-list--menu text-right work-sans-regular">
                            <nav>
                                <ul>
                                    <div class="list col-lg-9 left" id="menu">
                                        <li><a href="<?php bloginfo('url'); ?>" id="home">HOME</a></li>
                                        <li><a href="<?php bloginfo('url'); ?>/institucional" id="institucional">INSTITUCIONAL</a></li>
                                        <li><a href="<?php bloginfo('url'); ?>/edicoes" id="edicoes">EDIÇÕES</a></li>
                                        <li><a href="<?php bloginfo('url'); ?>/eventos" id="eventos">EVENTOS</a></li>
                                        <li><a href="<?php bloginfo('url'); ?>/materia" id="materias">MATÉRIAS</a></li>
                                        <li><a href="<?php bloginfo('url'); ?>/contato" id="contato">CONTATO</a></li>                                
                                    </div>
                                    <div class="option col-lg-3 left work-sans-regular">
                                        <li><a href="#seja" id="ancora">SEJA UM FRANQUEADO</a></li>
                                    </div>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-mobile mobile-show tablet-show col-xs-12">
            <header class="menu col-xs-2 col-md-1 left">
                  <div class="icon-menu" id="open-menu">
                      <s class="bar"></s>
                      <s class="bar"></s>
                      <s class="bar"></s>
                  </div>
            </header>
            <div class="logo col-xs-4 left">
                 <a href="<?php bloginfo('url'); ?>/home"><img src="<?php bloginfo('template_directory')?>/image/logo.png"></a>
            </div>
            <aside class="sidebar" id="close-menu">
                <div class="icon-close text-right">
                    <i class="fa fa-times" id="icon-close"></i>
                </div>
                <nav class="nav-menu myriad-regular text-right">
                    <ul>
                        <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/institucional">INSTITUCIONAL</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/edicoes">EDIÇÕES</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/eventos">EVENTOS</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/materia">MATÉRIAS</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li> 
                    </ul>
                </nav>
            </aside>
        </div>
    </div>
    <?php if(is_home()): ?>
            <script type="text/javascript">
                document.getElementById("home").setAttribute("class","active");
            </script>
        <?php else: ?>
            <?php if(is_page('institucional')): ?>
                <script>
                    document.getElementById("institucional").setAttribute("class","active");
                </script>
            <?php else: ?>
                <?php if(is_page('eventos')): ?>
                    <script>
                        document.getElementById("eventos").setAttribute("class","active");
                    </script>
                <?php else: ?>
                    <?php if(is_page('edicoes')): ?>
                        <script>
                            document.getElementById("edicoes").setAttribute("class","active");
                        </script>
                        <?php else: ?>
                            <?php if(is_page('materia')): ?>
                                <script>
                                    document.getElementById("materias").setAttribute("class","active");
                                </script>
                            <?php else: ?>
                                <?php if(is_page('contato')): ?>
                                    <script>
                                        document.getElementById("contato").setAttribute("class","active");
                                    </script>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>