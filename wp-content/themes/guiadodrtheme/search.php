<?php include('header.php'); ?>
    <div class="row col-lg-12 col-xs-12 materias page">
        <div class="container center">
          <div class="row materias-container">
              <?php
                  $s=get_search_query();
                  if($s != ""){
                    $args = array(
                        's' =>$s
                    );
                    
                  }
                    // The Query
                  $the_query = new WP_Query( $args );
                  if ( $the_query->have_posts() ) : 
              ?>
              <div class="materias-search">
                  <div class="work-sans-regular">
                      <h1>RESULTADO DA BUSCA</h1>
                  </div>
                  <div class="work-sans-light">
                      <h2>Matérias ou Profissionais</h2>
                  </div>
            </div>
              <!-- the loop -->  
              <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                  $postID = $post->ID;
                  $conteudo_mat = get_field('conteudo_materias', $postID);
                  $imagem_mat = get_field('imagem_materias', $postID);
                  $imagem_prof = get_field('imagem_perfil', $postID);
                  $imagem_ed = get_field('imagem_edicao', $postID);
              ?>
                  <div class="materias-posts page col-lg-4 col-md-4 left container">
                      <div class="posts-img">
                        <img src="<?php echo $imagem_mat; ?>">
                        <img src="<?php echo $imagem_prof; ?>">
                        <img src="<?php echo $imagem_ed; ?>">
                      </div>
                      <div class="posts-title work-sans-regular">
                        <h1><?php the_title(); ?></h1>
                      </div>
                      <div class="posts-content work-sans-light">
                        <p><?php echo getResumeContent($conteudo_mat); ?></p>
                      </div>
                      <div class="posts-link work-sans-regular text-center">
                        <a href="<?php the_permalink() ?>">VEJA MAIS</a>
                      </div>
                  </div>  
              <?php endwhile; ?>

              <?php else : ?>
                  <h3><?php _e( 'Nenhuma matéria ou profissional encontrado.' ); ?></h3>
              <?php endif; ?>

          </div>
        </div>
    </div>
    <?php include('seja.php'); ?>
<?php include('footer.php'); ?>