
  <div class="row col-lg-12 col-xs-12 seja pages">
    	<div class="container center">
    		<?php 
				the_post();
				$post = get_post('173');
				$postId = $post->ID;
				$titulo = get_field('titulo_seja', $postId);
				$conteudo = get_field('conteudo_seja', $postId);
			?>
    		<div class="seja-block center">
    			<div class="seja-title text-center work-sans-medium">
    				<h1><?php echo $titulo?></h1>
    			</div>
    		</div>
			<div class="seja-content col-lg-6 center text-center work-sans-light">
				<p><?php echo $conteudo?></p>
			</div>
			<div class="seja-form col-lg-6 center">
				<form method="post" id="contact">
					<div class="form-field">
						<input type="text" name="contact[nome]" placeholder="Nome" required>
					</div>
					<div class="form-field">
						<input type="email" name="contact[email]" placeholder="E-mail" required>
					</div>
					<div class="form-field">
						<input type="text" name="contact[telefone]" placeholder="Telefone" class="form-mask-js" data-mask="telefone" required>
					</div>
					<div class="form-field">
						<textarea placeholder="Mensagem" name="contact[mensagem]" required></textarea>
					</div>
					<div class="left mensagem-block">
						<div id="EnvioOk" class="mensagem">
							<p>Mensagem enviada com sucesso</p>
						</div>
						<div id="EnvioError" class="mensagem">
							<p>Ocorreu um erro</p>
						</div>
					</div>
					<div class="form-field work-sans-medium right">
						<button type="submit" name="enviar">ENVIAR</button>
					</div>
				</form>
			</div>
    	</div>
    </div>
