<?php include('header.php'); ?>
    <div class="row col-lg-12 col-xs-12 materias page">
        <div class="align container center">
            <div class="materias-title work-sans-regular  container">
                <h1>MATÉRIAS</h1>
            </div>
        	<div class="row materias-container">
               <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $query = "post_type=materia&post_status=publish&paged=$paged&posts_per_page=6";
                    query_posts($query); 
                ?>
                    <?php              
                          while(have_posts()):
                            the_post();
                            $post = get_post(); 
                            $postID = $post->ID; 
                            $conteudo_mat = get_field('conteudo_materias', $postID);
                            $imagem_mat = get_field('imagem_materias', $postID);
                            //wp_reset_postdata();                          
                     ?>
                			<div class="materias-posts page col-lg-4 col-md-4 left container">
                				<div class="posts-img">
                					<img src="<?php echo $imagem_mat; ?>">
                				</div>
                				<div class="posts-title work-sans-regular">
                					<h1><?php the_title(); ?></h1>
                				</div>
                				<div class="posts-content work-sans-light">
                					<p><?php echo getResumeContent($conteudo_mat); ?></p>
                				</div>
                				<div class="posts-link work-sans-regular text-center">
                					<a href="<?php the_permalink(); ?>">VEJA MAIS</a>
                				</div>
                			</div>
                    <?php 
                        endwhile; 
                        $paginationArgs = array('mid_size' => 4, 'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'textdomain' ), 'next_text' => __( '<i class="fa fa-angle-right"></i>', 'textdomain' ));
                        $pagination = get_the_posts_pagination($paginationArgs);
                    ?>                    
                    <div class="row col-lg-12 container">
                        <div class="voltar work-sans-medium col-lg-6 col-xs-6 left text-left">
                            <a href="<?php bloginfo('template_directory')?>/home">VOLTAR</a>
                        </div>
                        <div id="pagination" class="col-lg-6 col-xs-6 left text-right pagenation-style">
                            <div class="wp-pagenavi work-sans-medium"><?php echo $pagination; ?></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php include('seja.php'); ?>
<?php include('footer.php'); ?>