<?php
    // Definimos o gatilho 'youtube' para a função 'shortcode_youtube'
    add_shortcode('youtube', 'shortcode_youtube');
     
    // Função que retorna o HTML pelo Shotcode
    function shortcode_youtube($atts, $content=null)
    {
       extract(
          shortcode_atts(
             array(
                'height' => 350,
                'width'  => 575,
                'rel'    => 0,
                'showsearch' => 0,
                'showinfo'   => 0,
             ),
             $atts
          )
       );
       return '<iframe width="'.$width.'" height="'.$height.'" src="'.$content.'?rel='.$rel.'&amp;showsearch='.$showsearch.'&amp;showinfo='.$showinfo.'" frameborder="0"></iframe>';
    }
   /*
    ================================
      //RESUME CONTENT
    ================================
  */
    function getResumeContent($content){
      if(strlen($content) > 150){
        $content  = strip_tags($content);
              $content  = trim($content);
        $content = substr($content, 0, 150);
        $pos = strrpos($content, " ");
        $resume = substr($content, 0, $pos)."...";
      }else{
        $resume = $content;
      }
      return $resume;
    }
  /*
    ================================
      //RESUME CONTENT
    ================================
  */

    function get_adsense($atts) {
      return '<script type="text/javascript"><!--
      google_ad_client = "pub-546321545321589";
      /* 468x60, created 9/13/10 */
      google_ad_slot = "54321565498";
      google_ad_width = 468;
      google_ad_height = 60;
      //-->
      </script>
      <script type="text/javascript"
      src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
      </script>
      ';
}
add_shortcode('adsense', 'get_adsense');
?>