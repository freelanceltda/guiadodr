<?php include('header.php'); ?>
	

	<div class="row col-lg-12 col-xs-12 page-edicao">
      <div class="container center">
          <div class="edicao-title work-sans-regular container"> 
              <h1>NOSSAS EDIÇÕES</h1>
          </div>
          <div class="row edicao-posts">
              <div class="row edicao-row">
                <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $query = "post_type=edicao&post_status=publish&paged=$paged&posts_per_page=8";
                    query_posts($query); 
                    $paginationArgs = array('mid_size' => 4, 'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'textdomain' ), 'next_text' => __( '<i class="fa fa-angle-right"></i>', 'textdomain' ));
                   $pagination = get_the_posts_pagination($paginationArgs);                         
                    while(have_posts()):
                          the_post();
                          $post = get_post(); 
                          $postID = $post->ID; 
                          $imagem = get_field('imagem_edicao', $postID);
                          $script = get_field('campo_teste', $postID);
                  ?>
                      <div class="container col-lg-3 col-md-4 left align">
                          <a href="http://e.issuu.com/embed.html#<?php echo $script?>"  target="_blank">
                              <div class="edicao-posts--container">
                                  <div class="post-img">
                                      <img src="<?php echo $imagem; ?>">
                                  </div>
                              </div>
                          </a>
                      </div>
                  <?php endwhile; ?>
                  <div class="row col-lg-12 col-xs-12 ">
                      <div class="col-lg-6 col-xs-6 left container voltar work-sans-medium">
                          <a href="<?php bloginfo('template_directory')?>/home">VOLTAR</a>
                      </div>
                      <div class="col-lg-6 col-xs-6 pagenation-style left container text-right">
                          <div class="wp-pagenavi work-sans-medium"><?php echo $pagination; ?></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
	</div>




<?php include('seja.php'); ?>
<?php include('footer.php'); ?>