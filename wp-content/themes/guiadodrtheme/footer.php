		<div class="row col-lg-12 col-xs-12 footer">
			<div class="footer-first">
				<div class="container center">
				<?php 
					the_post();
					$post = get_post('184');
					$postId = $post->ID;
					$titulo_sobre = get_field('titulo_sobre', $postId);
					$conteudo_sobre = get_field('conteudo_sobre', $postId);
				?>
					<div class="footer-about col-lg-3 col-xs-12 col-md-6 left">
						<div class="footer-about--title work-sans-regular col-md-10">
							<h1><?php echo $titulo_sobre; ?></h1>
						</div>
						<div class="footer-about--content work-sans-light col-md-10">
							<p><?php echo $conteudo_sobre?></p>
						</div>
					</div>
					<?php 
						the_post();
						$post = get_post('155');
						$ID = $post->ID;
						$cidade = get_field('cidade_header', $ID);
						$tel_icon = get_field('icone_header_phone', $ID);
						$tel = get_field('telefone_header', $ID);
						$mail_icon = get_field('icone_header_email', $ID);
						$email = get_field('email_header', $ID);
						$social_icon = get_field('icone_social', $ID);
						$social = get_field('link_rede_social', $ID);
					?>
					<div class="footer-contact col-lg-3 col-xs-12 col-md-3 left">
						<div class="col-lg-11 right align">
							<div class="footer-contact--title work-sans-regular">
								<h1>CONTATO</h1>
							</div>
							<div class="row footer-contact--city">
								<p><?php echo $cidade; ?></p>
							</div>
							<div class="row footer-contact--phone">
								<i class="<?php echo $tel_icon; ?>"></i><p><?php echo $tel; ?></p>
							</div>
							<div class="row footer-contact--mail">
								<i class="<?php echo $mail_icon; ?>"></i><p><?php echo $email; ?></p>
							</div>
						</div>
						<div class="row footer-social col-lg-11 col-xs-12 right">
							<div class="row footer-social--title">
								<h1>SIGA-NOS</h1>
							</div>
							<div class="footer-social--icon">
								<a href="<?php echo $social; ?>"><i class="<?php echo $social_icon; ?>"></i></a>	
							</div>
						</div>
					</div>
					<div class="footer-menu col-lg-3 left col-xs-12 mobile-hide tablet-hide">
						<div class="footer-menu--title">
							<h1>MENU</h1>
						</div>
						<div class="footer-menu--list">
							<ul>
								<li><a href="<?php bloginfo('url'); ?>/homepage">HOME</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/institucional" id="institucional">INSTITUCIONAL</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/edicoes">EDIÇÕES</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/eventos">EVENTOS</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/materia">MATÉRIAS</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>  
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-secound work-sans-medium">
				<div class="text-center">
					<p>©2017 All Rights Reserved</p>
				</div>
			</div>
		</div>	

        <script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery-1.11.3.min.js"></script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	    <script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/nora.1.0.min.js"></script>
    </body>
</html>

<?php 	
	$post = get_post('155');
	$mail_ID = $post->ID;
	while ( have_rows('carousel_header') ) : the_row();
    	$email_contato = get_sub_field('email_header', $mail_ID);
    endwhile;
?>
<?php 
	//enviar
	  
	  // emails para quem será enviado o formulário

	if (isset($_REQUEST['contact']))
	{
	  $nome = $_REQUEST['contato']['nome'];
	  $email = $_REQUEST['contato']['email'];
	  $telefone = $_REQUEST['contato']['telefone'];
	  $mensagem = $_REQUEST['contato']['mensagem'];



	  $emailenviar = $email_contato;
	  //$emailenviar = "enriqueprieto@wdezoito.com.br";
	  $destino = $emailenviar;
	  $assunto = "Contato pelo Site";

	  // É necessário indicar que o formato do e-mail é html
	  $headers  = 'MIME-Version: 1.0' . "\r\n";
	  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	  $headers .= 'From: '.$nome.' <'.$destino.'>'. "\r\n";
	  $headers .= 'Reply-To: <'.$email.'>'. "\r\n";
	  //$headers .= "Bcc: $EmailPadrao\r\n";
	  
	  

	  $msg = "";
	  $msg .= "Nome: ".$nome."<br />";
	  $msg .= "E-mail: ".$email."<br />";
	  $msg .= "Telefone: ".$telefone."<br />";
	  $msg .= "Mensagem: ".$mensagem."<br />";


	  $enviaremail = mail($destino, $assunto, $msg, $headers);
	 
    if($enviaremail){
	  	?>
	  	  <script type="text/javascript">
	  	  $(document).ready(function(){
		  	  	document.getElementById("EnvioOk").style.display = "block";
	  	  		/*simpleAlert({text: 'E-mail enviado com sucesso.', theme: 'success', temporary: true});*/
	  	  })  	    
	  	  </script>  	  
		<?php 
	 } 
		else {
	?>
			<script type="text/javascript">
		  	  $(document).ready(function(){
		  	  		document.getElementById("EnvioError").style.display = "block";
		  	  		/*simpleAlert({text: 'Deu ruim.', theme: 'error', temporary: true});*/
		  	  })  	    
		  	  </script> 
		<?php
		  	$mgm = "ERRO AO ENVIAR E-MAIL!";
	  }
	}
?>
