<?php include('header.php'); ?>

	<div class="row col-lg-12 col-xs-12 materia-single">
		<?php while ( have_posts() ) : the_post();
			 $postID= $post->ID; 
             $conteudo_mat = get_field('conteudo_materias', $postID);
             $imagem_mat = get_field('imagem_materias', $postID);
             $imagem_background = get_field('imagem_background_materia', $postID);
	    ?>
		<div class="container center">
			<div class="materia-single--container">
				<div class="single-title">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="single-img">
					<img src="<?php echo $imagem_background; ?>">
				</div>
				<div  class="single-content">
					<p><?php echo $conteudo_mat; ?></p>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>

	<div class="materia-bkg--pages">
		<div class="container center">
			<div class="materia-title container work-sans-regular">
				<h1>ÚLTIMAS MATÉRIAS</h1>
			</div>		
		</div>
		<?php include('materia.php'); ?>
		<div class="container center">
			<div class="voltar work-sans-medium">
				<a href="<?php bloginfo('url')?>/home">VOLTAR</a>
			</div>
		</div>
	</div>
	<?php include('seja.php'); ?>

<?php include('footer.php'); ?>