    <div class="row col-lg-12 col-xs-12 materias">
    	<div class="materias-align">
	    	<div class="materias-block center">
	    		<div class="materias-block--title text-center work-sans-medium">
	    			<h1>MATÉRIAS</h1>
	    		</div>
	    	</div>
    	</div>
    	<div class="container center">
    		<div class="row materias-container">
                    <?php 
                        $matArgs = array(
                            'post_type' => 'materia', 
                            'showposts' => '3'
                        );                   
                                                
                              $matLoop = new WP_Query( $matArgs );                  
                                                
                              while ( $matLoop->have_posts() ) : $matLoop->the_post();
                                    $postID-> $post->ID; 
                                    $conteudo_mat =  get_field('conteudo_materias', $postID);
                                    $imagem_mat = get_field('imagem_materias', $postID);
                     ?>
                			<div class="materias-posts col-lg-4 col-xs-12 col-md-4 left container">
                				<div class="posts-img">
                					<img src="<?php echo $imagem_mat; ?>">
                				</div>
                				<div class="posts-title work-sans-regular">
                					<h1><?php the_title(); ?></h1>
                				</div>
                				<div class="posts-content work-sans-light">
                					<p><?php echo getResumeContent($conteudo_mat); ?></p>
                				</div>
                				<div class="posts-link work-sans-regular text-center">
                					<a href="<?php the_permalink(); ?>">VEJA MAIS</a>
                				</div>
                			</div>
                          
                    <?php endwhile; ?>
    		</div>
        </div>
    </div>