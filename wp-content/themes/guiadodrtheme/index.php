<?php include('header.php'); ?>
    <?php 
        $post = get_post('5');
        $home_Id = $post->ID;
        $home_bkg = get_field('banner_home', $home_Id);
        $boas_vindas = get_field('boas_vinda_home', $home_Id);
        $site_name = get_field('nome_do_site', $home_Id);
        $conteudo_home = get_field('conteudo_home', $home_Id);

    ?>
	<div class="row col-lg-12 col-xs-12 banner ">
        <div class="banner-container banner-bkg" style="background-image: url(<?php echo $home_bkg; ?>)">
            <div class="banner-content">
                <div class="container center">
                    <div class="banner-content--title col-lg-5">
                        <h1><?php echo $boas_vindas; ?><span><?php echo $site_name; ?></span></h1>
                    </div>
                    <div class="banner-content--text work-sans-light">
                        <p><?php echo $conteudo_home; ?></p>
                    </div>
                    <div class="banner-content--link work-sans-medium">
                        <a href="<?php bloginfo('url')?>/edicoes">VER EDIÇÃO ATUAL</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row banner-search container center">
            <div class=" row banner-search--container col-lg-10 center">
                <div class="search col-lg-6 col-xs-12 left">
                    <div class="search-title col-lg-12 work-sans-medium">
                        <h1>GUIA MÉDICO</h1>
                    </div>
                    <div class="search-content col-lg-12 work-sans-regular">
                        <p>Busque por médicos, e profissionais de saúde.</p>
                    </div>
                    <div class="search-form col-lg-12 ">
                        <form id="searchMat" method="get" action="<?php echo home_url(); ?>" role="search">
                            <div class="form-field col-lg-9 col-xs-9 left">
                                <input type="search" name="s" placeholder="Profissionais ou especialidades" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
                            </div>
                            <div class="form-field col-lg-3 col-xs-3 right text-right work-sans-regular">
                                <button id="btnSearchMat"  role="button"  type="submit">BUSCAR</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="search col-lg-6 col-xs-12 left">
                    <div class="search-title work-sans-medium">
                        <h1>MATÉRIAS</h1>
                    </div>
                    <div class="search-content work-sans-regular">
                        <p>Leia as melhores matérias.</p>
                    </div>
                    <div class="search-form">
                        <form id="searchMat" method="get" action="<?php echo home_url(); ?>" role="search">
                            <div class="form-field col-lg-9 col-xs-9 left">
                                <input type="search" name="s" placeholder="Matérias" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
                            </div>
                            <div class="form-field col-lg-3 col-xs-3 right text-right work-sans-regular">
                                <button id="btnSearchMat"  role="button"  type="submit">BUSCAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row col-lg-12 col-xs-12 edicao">
        <div class="container center">
            <div class="edicao-block center">
                <div class="edicao-block--title text-center work-sans-medium">
                    <h1>NOSSAS EDIÇÕES</h1>
                </div>
            </div>
            <div class="row edicao-posts">
                <div class="container col-xs-12 col-md-12">
                	<button id="prev-edicao"><i class="fa fa-angle-left"></i></button>
                    <div id="carousel-edicao" class="carousel-edicao col-lg-12 col-xs-12">
                        <?php 
                            $edicaoArgs = array(
                                'post_type' => 'edicao', 
                            );                   
                                                       
                                  $edicaoLoop = new WP_Query( $edicaoArgs );                  
                                                    
                                  while ( $edicaoLoop->have_posts() ) : $edicaoLoop->the_post();
                                        $postID-> $post->ID; 
                                        $imagem = get_field('imagem_edicao', $postID);
                                        $script = get_field('campo_teste', $postID);

                        ?>
                                <div class="align container">
                                    <a href="http://e.issuu.com/embed.html#<?php echo $script?>"  target="_blank">
                                    	<div class="edicao-posts--container">
                	                        <div class="post-img">
                	                            <img src="<?php echo $imagem; ?>">
                	                        </div>
                	                        <div class="posts-link text-center work-sans-regular">
                	                        	<a href="http://e.issuu.com/embed.html#<?php echo $script?>" target="_blank">VER EDIÇÃO</a>
                	                        </div>
                                    	</div>
                                    </a>
                                </div>
                        <?php endwhile; ?>
                    </div>
                    <button id="next-edicao"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="edicao-posts--link text-center work-sans-medium">
                	<a href="<?php bloginfo('url')?>/edicoes">TODAS AS EDIÇÕES</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row col-lg-12 col-xs-12 profissional">
        <div class="container center">
            <div class="profissional-block center">
                <div class="profissional-block--title text-center work-sans-medium">
                    <h1>PROFISSIONAIS</h1>
                </div>
            </div>
            <div class="row profissional-posts container">
                <div class="container col-xs-12">
                    <button id="prev-profissional"><i class="fa fa-angle-left"></i></button>
                    <div id="carousel-profissional" class="carousel-profissional col-lg-12">
                        <?php 
                            $profArgs = array(
                                'post_type' => 'profissional', 
                            );                   
                                                    
                                  $profLoop = new WP_Query( $profArgs );                  
                                                    
                                  while ( $profLoop->have_posts() ) : $profLoop->the_post();
                                        $postID-> $post->ID; 
                                        $especialidade = get_field('especialidade_profissionais', $postID);
                                        $imagem = get_field('imagem_profissionais', $postID);
                        ?>
                            <div class="align container">
                                <div class="profissional-posts--container">
                                    <div class="posts-img">
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo $imagem; ?>">
                                            <div class="posts-content">
                                                <div class="content-nome col-lg-8 col-xs-10 text-center center">
                                                    <h2><?php the_title(); ?></h2>
                                                </div>
                                                <div class="content-esp col-lg-8 col-xs-10 text-center center work-sans-light">
                                                    <p><?php echo $especialidade; ?></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="posts-link text-center work-sans-regular">
                                        <a href="<?php the_permalink(); ?>">VER PERFIL</a>
                                    </div>
                                </div>
                            </div>
                      <?php endwhile; ?>
                    </div>  
                    <button id="next-profissional"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="profissional-posts--link text-center work-sans-medium">
                    <a href="<?php bloginfo('url')?>/profissional">PROFISSIONAIS</a>
                </div>
            </div>
        </div>
    </div>
    <?php include('materia.php'); ?>
    <a name="seja"></a>
    <?php include('seja.php'); ?>


<?php include('footer.php'); ?>