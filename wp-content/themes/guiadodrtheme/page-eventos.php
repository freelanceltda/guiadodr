<?php include('header.php'); ?>

 <div class="row col-lg-12 col-xs-12 eventos">
	<div class="align container center">
   		 <div class="col-lg-12 col-xs-12 eventos-block">
			<div class="eventos-container container col-lg-6 left">
				<div class="eventos-title work-sans-regular">
					<h1>EVENTOS</h1>
				</div>
			</div>
   		 </div>
		<div class="row col-lg-12 col-xs-12 eventos-align">
			<?php           
			  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $query = "post_type=evento&post_status=publish&paged=$paged&posts_per_page=6";
              query_posts($query); 
              $paginationArgs = array('mid_size' => 4, 'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'textdomain' ), 'next_text' => __( '<i class="fa fa-angle-right"></i>', 'textdomain' ));
        	  $pagination = get_the_posts_pagination($paginationArgs);                         
              while(have_posts()):
                    the_post();
                    $post = get_post(); 
                    $postID = $post->ID; 
                    $conteudo_event = get_field('conteudo_evento', $postID);
                    $imagem_event = get_field('imagem_evento', $postID);
	         ?>
	    			<div class="evento-posts page col-lg-4 col-md-4 left container">
	    				<div class="posts-img">
	    					<img src="<?php echo $imagem_event; ?>">
	    				</div>
	    				<div class="posts-title work-sans-regular">
	    					<h1><?php the_title(); ?></h1>
	    				</div>
	    				<div class="posts-content work-sans-light">
	    					<p><?php echo getResumeContent($conteudo_event); ?></p>
	    				</div>
	    				<div class="posts-link work-sans-regular text-center">
	    					<a href="<?php the_permalink(); ?>">VEJA MAIS</a>
	    				</div>
	    			</div>
	        <?php 
	            endwhile; 
	        ?>
	        <div class="row col-lg-12 col-xs-12 ">
	        	<div class="col-lg-6 col-xs-6 left container voltar work-sans-medium">
	        		<a href="<?php bloginfo('template_directory')?>/home">VOLTAR</a>
	        	</div>
	        	<div class="col-lg-6 col-xs-6 pagenation-style left container text-right">
	        		<div class="wp-pagenavi work-sans-medium"><?php echo $pagination; ?></div>
	        	</div>
	        </div>
		</div>
	</div>
</div>







<?php include('seja.php'); ?>
<?php include('footer.php'); ?>