<?php include('header.php'); ?>

	<div class="row col-lg-12 col-xs-12">
		<div class="container center">
			<div class="row single-profissional">
			    <?php 
			    		 the_post();
						 $post = get_post();        
						 $postId = $post->ID;
		    			 $imagem = get_field('imagem_profissionais', $postId);
		    			 $imagem_perfil = get_field('imagem_perfil', $postID);
		    			 $especialidade = get_field('especialidade_profissionais', $postId);
		    			 $crm = get_field('crm_profissionais', $postId);
		    			 $cidade_crm = get_field('cidade_crm_profissionais', $postId);
		    			 $endereco = get_field('endereco_profissionais', $postId);
		    			 $cidade = get_field('cidade_profissionais', $postId);
		    			 $telefone = get_field('telefone_profissionais', $postId);
			     ?>
				<div class="single-profissional--container col-lg-12 left">
					<div class="col-lg-3">
						<div class="profissional-img">
							<img src="<?php echo $imagem_perfil; ?>">
						</div>
					</div>
					<div class="col-lg-6 left work-sans-regular">
						<div class="profissional-nome">
		       			 	<h1><?php the_title(); ?></h1>
						</div>
						<div class="profissional-esp">
							<p><?php echo $especialidade; ?></p>
						</div>
						<div class="row col-lg-12 col-xs-12"> 
							<div class="profissional-crm left">
								<p><?php echo $crm; ?></p>
							</div>
							<div class="profissional-cidade--crm left">
								<p><?php echo $cidade_crm; ?></p>
							</div>
						</div>
						<div class="row col-lg-12 col-xs-12">
							<div class="profissional-end left">
								<p><?php echo $endereco; ?></p>
							</div>
							<div class="profissional-cidade left">
								<p><?php echo $cidade; ?></p>
							</div>
						</div>
						<div class="profissional-phone">
							<p><?php echo $telefone; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<?php include('seja.php'); ?>


<?php include('footer.php'); ?>