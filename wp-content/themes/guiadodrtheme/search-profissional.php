<?php include('header.php'); ?>

    <div class="row col-lg-12 col-xs-12">
        <div class="container center">
            <div class="row single-profissional">      
              <?php
                  $p=get_search_query();
                  $args = array(
                      'p' =>$p
                  );
                    // The Query
                  $the_query = new WP_Query( $args );
                  if ( $the_query->have_posts() ) : 
              ?>
              <!-- the loop -->  
              <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                  $postID = $post->ID;
                  $imagem = get_field('imagem_profissionais', $postID);
                  $especialidade = get_field('especialidade_profissionais', $postID);
                  $crm = get_field('crm_profissionais', $postID);
                  $cidade_crm = get_field('cidade_crm_profissionais', $postID);
                  $endereco = get_field('endereco_profissionais', $postID);
                  $cidade = get_field('cidade_profissionais', $postID);
                  $telefone = get_field('telefone_profissionais', $postID); 
              ?>
                  <div class="row single-profissional--container page col-lg-12">
                      <div class="col-lg-4">
                          <div class="profissional-img">
                              <img src="<?php echo $imagem; ?>">
                          </div>
                      </div>
                      <div class="col-lg-6 left work-sans-regular">
                          <div class="profissional-nome">
                              <h1><?php the_title(); ?></h1>
                          </div>
                          <div class="profissional-esp">
                              <p><?php echo $especialidade; ?></p>
                          </div>
                          <div class="row col-lg-12 col-xs-12"> 
                              <div class="profissional-crm left">
                                  <p><?php echo $crm; ?></p>
                              </div>
                              <div class="profissional-cidade--crm left">
                                  <p><?php echo $cidade_crm; ?></p>
                              </div>
                          </div>
                          <div class="row col-lg-12 col-xs-12">
                              <div class="profissional-end left">
                                  <p><?php echo $endereco; ?></p>
                              </div>
                              <div class="profissional-cidade left">
                                  <p><?php echo $cidade; ?></p>
                              </div>
                          </div>
                          <div class="profissional-phone">
                              <p><?php echo $telefone; ?></p>
                          </div>
                      </div>
                  </div>
              <?php endwhile; ?>

              <?php else : ?>
                  <h3><?php _e( 'Nenhum profissional encontrado.' ); ?></h3>
              <?php endif; ?>
            </div>
        </div>
    </div>

<?php include('seja.php'); ?>

<?php include('footer.php'); ?>
