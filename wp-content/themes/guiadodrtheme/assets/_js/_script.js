$(document).ready(function(){
    ImagensUx();
    book();
	var carouselEdicao = $('#carousel-edicao');
    carouselEdicao.owlCarousel({
		items : 4,
		slideSpeed: 300,
		paginationSpeed: 400, 
		loop: true,
		responsive:{
            320:  {
                items:1
            },
            580:{
                items:2
            },
            810:{
                items:3
            },
        }
	});

    $('#next-edicao').click(function(){
        carouselEdicao.trigger('next.owl.carousel');
    });
    $('#prev-edicao').click(function(){
        carouselEdicao.trigger('prev.owl.carousel');
    });

	var carouselProf = $('#carousel-profissional');
    carouselProf.owlCarousel({
		items : 3,
		slideSpeed: 300,
		paginationSpeed: 400, 
		loop: true,
		responsive:{
            320:  {
                items:1
            },
            580:{
                items:2
            },
            810:{
                items:3
            },
        }
	});
    $('#prev-profissional').click(function(){
        carouselProf.trigger('prev.owl.carousel');
    });
    $('#next-profissional').click(function(){
        carouselProf.trigger('next.owl.carousel');
    });


    var carouselEvent = $('#carousel-eventos');
    carouselEvent.owlCarousel({
        items : 4,
        slideSpeed: 300,
        paginationSpeed: 400, 
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive:{
            320:  {
                items:1
            },
            450:{
                items:2
            },
            767:{
                items:3
            },
            980:{
                items:4
            },
        }
    });

    $(window).scroll(function(){
        var top = ($(window).scrollTop());
        if(top >= 150){
            $('.header-list').addClass('header-scroll');
            $('.header-info').css({'display':'none'});
            $('.header-mobile').addClass('header-mobile--scroll');
        }
        else
        {
            $('.header-info').css({'display':'block'});
            $('.header-list').removeClass('header-scroll');
            if(top = 150){
                $('.header-mobile').removeClass('header-mobile--scroll');
            }
        }
    });

    $('#Pesquisa-header--mobile input').keydown(function(e) {
        if (e.keyCode == 13) {
            $('#Pesquisa-header--mobile').submit();
        }
    });

    /*
        #######################
            MENU RESPONSIVE
        #######################
    */
            $('#open-menu').click(function(){
                if($(this).hasClass('mov')){
                    $(this).removeClass('mov');
                    $('#close-menu').css({'left' : '-100%'});
                }else{
                    $(this).addClass('mov');
                    $('#close-menu').css({'left' : '0'});
                }
            });

            $("#icon-close").click(function(){
                $(".icon-menu").removeClass('mov');
                $('#close-menu').css({'left' : '-100%'});
            });

            $(window).scroll(function(){
                $(this).removeClass('mov');
                $('#close-menu').css({'left' : '-100%'});
            });
    /*
        #######################
           //MENU RESPONSIVE
        #######################
    */

    
     $('#ancora').click(function(){
        $('html, body').animate({
            scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top - 180
        }, 500);
        return false;
    });

});


function ImagensUx(){
    $('.carousel-img ').on('click', function(){
        var img =  $(this).attr('data-image');

        $("#modalImg").attr('src', img);
        console.log(img);

        var modal = new Modal(),
        clone = $("#Modal").clone();
        modal.setContainer(clone[0]);
        modal.setClassModalContainer("open");
        modal.setClassModalContainer("center");
        modal.showModal();

    });
}

function book(){
    $('#mybook').booklet();
}