 <?php include('header.php'); ?>

    <div class="row col-lg-12 col-xs-12">
        <div class="container center">
            <div class="row single-profissional">
                <?php            
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $query = "post_type=profissional&post_status=publish&paged=$paged&posts_per_page=3";
                    query_posts($query); 
                    $paginationArgs = array('mid_size' => 4, 'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'textdomain' ), 'next_text' => __( '<i class="fa fa-angle-right"></i>', 'textdomain' ));
                    $pagination = get_the_posts_pagination($paginationArgs);    
                    while ( have_posts()):
                         the_post();
                         $post = get_post(); 
                         $postID = $post->ID; 
                         $imagem = get_field('imagem_profissionais', $postID);
                         $imagem_perfil = get_field('imagem_perfil', $postID);
                         $especialidade = get_field('especialidade_profissionais', $postID);
                         $crm = get_field('crm_profissionais', $postID);
                         $cidade_crm = get_field('cidade_crm_profissionais', $postID);
                         $endereco = get_field('endereco_profissionais', $postID);
                         $cidade = get_field('cidade_profissionais', $postID);
                         $telefone = get_field('telefone_profissionais', $postID); 
                ?>
                    <div class="row single-profissional--container page col-lg-12">
                        <div class="col-lg-3">
                            <div class="profissional-img">
                                <img src="<?php echo $imagem_perfil; ?>">
                            </div>
                        </div>
                        <div class="col-lg-6 left work-sans-regular">
                            <div class="profissional-nome">
                                <h1><?php the_title(); ?></h1>
                            </div>
                            <div class="profissional-esp">
                                <p><?php echo $especialidade; ?></p>
                            </div>
                            <div class="row col-lg-12 col-xs-12"> 
                                <div class="profissional-crm left">
                                    <p><?php echo $crm; ?></p>
                                </div>
                                <div class="profissional-cidade--crm left">
                                    <p><?php echo $cidade_crm; ?></p>
                                </div>
                            </div>
                            <div class="row col-lg-12 col-xs-12">
                                <div class="profissional-end left">
                                    <p><?php echo $endereco; ?></p>
                                </div>
                                <div class="profissional-cidade left">
                                    <p><?php echo $cidade; ?></p>
                                </div>
                            </div>
                            <div class="profissional-phone">
                                <p><?php echo $telefone; ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <div class="row col-lg-12 col-xs-12 ">
                    <div class="col-lg-6 left container voltar work-sans-medium">
                        <a href="<?php bloginfo('template_directory')?>/home">VOLTAR</a>
                    </div>
                    <div class="col-lg-6 pagenation-style left container text-right">
                        <div class="wp-pagenavi work-sans-medium">
                            <?php echo $pagination; ?>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('seja.php'); ?>

<?php include('footer.php'); ?>